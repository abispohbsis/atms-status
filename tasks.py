# Open a terminal and run the following command (development only!):
# celery -A tasks worker -B --loglevel=INFO

import uuid
import pika
import json
from celery import Celery

QUEUE_NAME = 'atms_queue'

app = Celery('tasks', broker='amqp://rabbitmq:rabbitmq@localhost:5672')

credentials = pika.PlainCredentials('rabbitmq', 'rabbitmq')


def publish(data):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host="localhost", credentials=credentials)
    )

    channel = connection.channel()
    channel.queue_declare(queue=QUEUE_NAME, arguments={'x-message-ttl': 60 * 1000})

    message = json.dumps(data)
    channel.basic_publish(
        exchange='',
        routing_key=QUEUE_NAME,
        body=message,
    )
    connection.close()


@app.task
def atm1():
    data = {
        'id': str(uuid.uuid4()),
        'balance': 400,
        'banknotes': {
            '20': 0,
            '50': 4,
            '100': 2
        }
    }

    publish(data)


@app.task
def atm2():
    data = {
        'id': str(uuid.uuid4()),
        'balance': 50,
        'banknotes': {
            '20': 0,
            '50': 1,
            '100': 0
        }
    }

    publish(data)


@app.task
def atm3():
    data = {
        'id': str(uuid.uuid4()),
        'balance': 1000,
        'banknotes': {
            '20': 20,
            '50': 4,
            '100': 4
        }
    }

    publish(data)


@app.task
def atm4():
    data = {
        'id': str(uuid.uuid4()),
        'balance': 0,
        'banknotes': {
            '20': 0,
            '50': 0,
            '100': 0
        }
    }

    publish(data)


@app.task
def atm5():
    data = {
        'id': str(uuid.uuid4()),
        'balance': 100,
        'banknotes': {
            '20': 0,
            '50': 4,
            '100': 1
        }
    }

    publish(data)


app.conf.beat_schedule = {
    "amt-1": {"task": "tasks.atm1", "schedule": 10.0},
    "amt-2": {"task": "tasks.atm2", "schedule": 10.0},
    "amt-3": {"task": "tasks.atm3", "schedule": 10.0},
    "amt-4": {"task": "tasks.atm4", "schedule": 10.0},
    "amt-5": {"task": "tasks.atm5", "schedule": 10.0},
}

if __name__ == '__main__':
    data = {
        'id': str(uuid.uuid4()),
        'message': 'OK'
    }

